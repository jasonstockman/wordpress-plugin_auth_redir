<?php
/*
 * Plugin Name: Auth Redirect Override
 * Version: 1.0.0
 * Description: Masks the IP
 * Author: Jason
*/

// Override auth_redirect in pluggable.php
if( ! function_exists('auth_redirect') ) {
	function auth_redirect() {

		$domain = 'https://protonvpn.com';
		$redirect = ( strpos( $_SERVER['REQUEST_URI'], '/options.php' ) && wp_get_referer() ) ? wp_get_referer() : set_url_scheme( $domain . $_SERVER['REQUEST_URI'] );
		$login_url = wp_login_url($redirect, true);
		$secure = ( is_ssl() || force_ssl_admin() );
		$secure = apply_filters( 'secure_auth_redirect', $secure );

		if ( $secure && !is_ssl() && false !== strpos($_SERVER['REQUEST_URI'], 'wp-admin') ) {
			if ( 0 === strpos( $_SERVER['REQUEST_URI'], 'http' ) ) {
				wp_redirect( set_url_scheme( $_SERVER['REQUEST_URI'], 'https' ) );
				exit();
			} else {
				wp_redirect( $domain . $_SERVER['REQUEST_URI'] );
				exit();
			}
		}

		$scheme = apply_filters( 'auth_redirect_scheme', '' );
		if ( $user_id = wp_validate_auth_cookie( '',  $scheme) ) {
			do_action( 'auth_redirect', $user_id );
			if ( !$secure && get_user_option('use_ssl', $user_id) && false !== strpos($_SERVER['REQUEST_URI'], 'wp-admin') ) {
				if ( 0 === strpos( $_SERVER['REQUEST_URI'], 'http' ) ) {
					wp_redirect( set_url_scheme( $_SERVER['REQUEST_URI'], 'https' ) );
					exit();
				} else {
					wp_redirect( $domain . $_SERVER['REQUEST_URI'] );
					exit();
				}
			}
			return;
		}
		nocache_headers();
		wp_redirect($login_url);
		exit();
	}
}